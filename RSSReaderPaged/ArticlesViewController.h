//
//  ArticlesViewController.h
//  RSSReaderPaged
//
//  Created by Mac on 22.02.16.
//  Copyright © 2016 zuzex. All rights reserved.
//

#import "FeedsViewController.h"
#import "DetailViewController.h"
#import "MBProgressHUD.h"
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface ArticlesViewController : UITableViewController <NSXMLParserDelegate>{
    NSMutableArray *feeds;
    NSMutableDictionary *item;
    NSMutableString *element;
    NSMutableString *title;
    NSMutableString *link;
    NSMutableString *descr;
    NSMutableString *pubDate;
}

@property (strong, nonatomic) NSMutableArray *urlsOfArticles;
@property (strong, nonatomic) NSMutableArray *localStorage;
@end
