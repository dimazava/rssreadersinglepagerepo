  //
//  ArticlesViewController.m
//  RSSReaderPaged
//
//  Created by Mac on 22.02.16.
//  Copyright © 2016 zuzex. All rights reserved.
//

#import "ArticlesViewController.h"

@interface ArticlesViewController ()

@end

@implementation ArticlesViewController
@synthesize localStorage;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Entity"];
    localStorage = [[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
    NSArray *condition = [localStorage valueForKey:@"isAppLaunchedOnce"];
    
    if ([condition[0] isEqual: @"YES"]){
        [self.urlsOfArticles removeAllObjects];
        NSObject *recievedObj = [localStorage valueForKey:@"cellSelected"];
        NSString *stringsOfUrls = [recievedObj description];
        NSCharacterSet *unwantedChars = [NSCharacterSet characterSetWithCharactersInString:@"\"(),"];
        stringsOfUrls = [[stringsOfUrls componentsSeparatedByCharactersInSet:unwantedChars] componentsJoinedByString:@" "];
        NSMutableArray *setOfURLs = [stringsOfUrls componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        [setOfURLs removeObject:@""];
        self.urlsOfArticles = setOfURLs;
    }
    
    //[self.tableView registerClass:[UITableView class] forCellReuseIdentifier:@"Cell"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self fillUpTableViewWithArticles:[self urlsOfArticles]];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)fillUpTableViewWithArticles:(NSArray*)urlsOfSelectedFeeds{
    feeds = [NSMutableArray array];
    NSXMLParser *parser;
    NSURL *url;
    for (NSString *curUrl in urlsOfSelectedFeeds)
    {
        if (![curUrl isEqual:@""]){
            url = [NSURL URLWithString:[curUrl stringByReplacingOccurrencesOfString:@" " withString:@""]];
            parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
            [parser setDelegate:self];
            [parser setShouldResolveExternalEntities:NO];
            [parser parse];
        }
    }
}

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary<NSString *,NSString *> *)attributeDict{
    element = [elementName mutableCopy];
    if ([element isEqualToString:@"item"]){
        item = [NSMutableDictionary dictionary];
        title = [NSMutableString string];
        link = [NSMutableString string];
        descr = [NSMutableString string];
        pubDate = [NSMutableString string];
    }
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    if ([element isEqualToString:@"title"]){
        [title appendString:string];
    }
    else if([element isEqualToString:@"link"]){
        [link appendString:string];
    } else if([element isEqualToString:@"description"]){
        [descr appendString:string];
    } else if([element isEqualToString:@"pubDate"]){
        [pubDate appendString:string];
    }
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    if ([elementName isEqualToString:@"item"]){
        [item setObject:title forKey:@"title"];
        [item setObject:link forKey:@"link"];
        [item setObject:descr forKey:@"description"];
        [item setObject:pubDate forKey:@"pubDate"];
        [feeds addObject:item];
    }
}

-(void)parserDidEndDocument:(NSXMLParser *)parser{
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return feeds.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    // Configure the cell...
    
    cell.textLabel.text = [[feeds objectAtIndex:indexPath.row] objectForKey:@"title"];
    return cell;
}

//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    [self performSegueWithIdentifier:@"showDetail" sender:self];
//}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        DetailViewController  *destViewController = segue.destinationViewController;
        destViewController.recievedNews = feeds[indexPath.row];;
    }
}

-(BOOL)prefersStatusBarHidden{
    return NO;
}

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]){
        context = [delegate managedObjectContext];
    }
    return context;
}

@end
