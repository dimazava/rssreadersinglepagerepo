//
//  DetailViewController.h
//  RSSReaderPaged
//
//  Created by Mac on 23.02.16.
//  Copyright © 2016 zuzex. All rights reserved.
//

#import "ArticlesViewController.h"
#import "WebReaderViewController.h"
#import "MBProgressHUD.h"
#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) NSArray *recievedNews;

@property (strong, nonatomic) IBOutlet UITextView *textPreviewView;

@end
