//
//  DetailViewController.m
//  RSSReaderPaged
//
//  Created by Mac on 23.02.16.
//  Copyright © 2016 zuzex. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

@synthesize recievedNews;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self getObjects];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });


    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void) getObjects{
    self.title = @"Article ";
    NSMutableAttributedString *titleFormatted;
    
    NSMutableAttributedString *dateFormatted;
    
    NSAttributedString *markup = [recievedNews valueForKey:@"description"];

    NSScanner *scanner = [NSScanner scannerWithString:markup];
    NSString *imageString, *descriptionString;
    descriptionString = [self stripTags:markup];
    
    [scanner scanUpToString:@"src=\"" intoString:nil];
    [scanner scanString:@"src=\"" intoString:nil];
    [scanner scanUpToString:@"\"" intoString:&imageString];
    
//    RTLabel *attr =[[RTLabel alloc] initWithFrame:CGRectZero];
    //[self.textPreviewView addSubview:attr];
//    [attr setText:markup];
    
    NSRange replaceRange = [descriptionString rangeOfString:@"&rarr;"];
    if (replaceRange.location != NSNotFound){
        descriptionString = [descriptionString stringByReplacingCharactersInRange:replaceRange withString:@""];
    }
    
    NSString *plainTempTitle = [recievedNews valueForKey:@"title"];
    replaceRange = [plainTempTitle rangeOfString:@"\t\t"];
    if (replaceRange.location != NSNotFound){
        plainTempTitle = [plainTempTitle stringByReplacingCharactersInRange:replaceRange withString:@"\t"];
    }
    titleFormatted = [[NSMutableAttributedString alloc] initWithString:plainTempTitle];
    [titleFormatted addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Helvetica-Bold" size:30.0f] range:NSMakeRange(0, [titleFormatted length] - 1)];
    
    NSString *plainTempData = [recievedNews valueForKey:@"pubDate"];
    replaceRange = [plainTempData rangeOfString:@"\t\t"];
    if (replaceRange.location != NSNotFound){
        plainTempData = [plainTempData stringByReplacingCharactersInRange:replaceRange withString:@"\t"];
    }

    dateFormatted =[[NSMutableAttributedString alloc] initWithString:plainTempData attributes: @{NSFontAttributeName: [UIFont italicSystemFontOfSize: 12.0]}];
    
    [self.textPreviewView setScrollEnabled:NO];
    NSMutableAttributedString *descriptionFormatted = [[NSMutableAttributedString alloc] initWithString:descriptionString];
    [descriptionFormatted addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Helvetica" size:18.0f] range:NSMakeRange(0, [descriptionFormatted length] - 1)];
    
    NSURL *urlImage = [NSURL URLWithString:imageString];
    [descriptionFormatted insertAttributedString:dateFormatted atIndex:0];
    if (urlImage != nil) {
        NSData *data = [NSData dataWithContentsOfURL:urlImage];
        NSTextAttachment *myAttachment = [[NSTextAttachment alloc] init];
        myAttachment.image = [UIImage imageWithData:data];
        
        CGFloat oldWidth = myAttachment.image.size.width;
        
        CGFloat scaleFactor = oldWidth / (self.textPreviewView.frame.size.width - 10);
        
        myAttachment.image = [UIImage imageWithCGImage:myAttachment.image.CGImage scale:scaleFactor orientation:UIImageOrientationUp];
        
        NSAttributedString *attrStringWithImage = [NSAttributedString attributedStringWithAttachment:myAttachment];

        [descriptionFormatted insertAttributedString:attrStringWithImage atIndex:0];
    }
    [descriptionFormatted insertAttributedString:titleFormatted atIndex:0];
    
    self.textPreviewView.attributedText = descriptionFormatted;
    
    [self.textPreviewView setScrollEnabled:YES];
    
    //TO DO - НАВЕСТИ МАРАФЕТ!!!!!
    
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"Открыть" style:UIBarButtonItemStylePlain target:self action:@selector(buttonClicked:)];
    
    self.navigationItem.rightBarButtonItem = anotherButton;
}

-(NSString *)stripTags:(NSString *)str{
    NSMutableString *html = [NSMutableString stringWithCapacity:[str length]];
    NSScanner *scanner = [NSScanner scannerWithString:str];
    NSString *tempText = nil;
    
    while (![scanner isAtEnd]) {
        [scanner scanUpToString:@"<" intoString:&tempText];
        
        if (tempText != nil) {
            [html appendString:tempText];
        }
        
        [scanner scanUpToString:@">" intoString:NULL];
        
        if (![scanner isAtEnd])
            [scanner setScanLocation:[scanner scanLocation] + 1];
        
        tempText = nil;
    }
    return html;
}

- (void)buttonClicked:(id)sender{
    [self performSegueWithIdentifier:@"showInReader" sender:sender];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showInReader"])
    {
        WebReaderViewController *destViewController = segue.destinationViewController;
        
        destViewController.urlOfPage = [recievedNews valueForKey:@"link"];
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
