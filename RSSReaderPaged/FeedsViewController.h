//
//  FeedsViewController.h
//  RSSReaderPaged
//
//  Created by Mac on 22.02.16.
//  Copyright © 2016 zuzex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArticlesViewController.h"
#import "MBProgressHUD.h"

@interface FeedsViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) NSMutableArray *cellSelected;
@property (strong, nonatomic) NSMutableArray *localStorage;

@end

