//
//  FeedsViewController.m
//  RSSReaderPaged
//
//  Created by Mac on 22.02.16.
//  Copyright © 2016 zuzex. All rights reserved.
//

#import "FeedsViewController.h"

@interface FeedsViewController (){
    NSArray *webSitesURL;
    NSArray *webSitesName;
}

@end

@implementation FeedsViewController
@synthesize cellSelected;
@synthesize localStorage;

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Entity"];
    localStorage = [[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
    NSArray *objPath = [localStorage valueForKey:@"selectedCellsPath"];
    NSArray *condition = [localStorage valueForKey:@"isAppLaunchedOnce"];
    
    if (([condition count] > 0) && ([condition[0] isEqualToString: @"YES"])){
        @try {
            if ([objPath[0] count] == 0)
                self.cellSelected = objPath[1];
            else
                self.cellSelected = objPath[0];
//            NSArray *arrayOfStrings = [pathData componentsSeparatedByString:@"!"];
//            self.cellSelected = [arrayOfStrings mutableCopy];
            
        }
        @catch (NSException *exception) {
            NSLog(@"%@", exception.reason);
        }
        [self.tableView reloadData];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [self getUrlsFromSite];
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(performPassingData:)];
            self.navigationItem.rightBarButtonItem = doneButton;
        });
    });

    // Do any additional setup after loading the view, typically from a nib.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Entity"];
    self.localStorage = [[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
}

-(void)performPassingData:(id)sender{
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Entity"];
    localStorage = [[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
    NSObject *obj = [localStorage valueForKey:@"isAppLaunchedOnce"];
    
    NSManagedObject *newData = [NSEntityDescription insertNewObjectForEntityForName:@"Entity" inManagedObjectContext:context];
    
    NSMutableArray *urlsOfSelectedFeeds = [NSMutableArray array];
    for (NSIndexPath *curCellPath in self.cellSelected){
        [urlsOfSelectedFeeds addObject:[webSitesURL objectAtIndex:curCellPath.row]];
    }
    NSString* data = [[urlsOfSelectedFeeds valueForKey:@"description"] componentsJoinedByString:@" "];
    NSArray* pathData = self.cellSelected;
    
    NSString *condition = [[[obj description] componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] componentsJoinedByString:@""];
    NSCharacterSet *unwantedChars = [NSCharacterSet characterSetWithCharactersInString:@"\"(),"];
    condition = [[condition componentsSeparatedByCharactersInSet:unwantedChars] componentsJoinedByString:@""];
    [newData setValue:@"YES" forKey:@"isAppLaunchedOnce"];
    [newData setValue:pathData forKey:@"selectedCellsPath"];
    NSError *error = nil;
    if(![context save:&error]){
        NSLog(@"Can't save! %@ %@", error, [error localizedDescription]);
    }
    NSString *tmp = [[[[localStorage valueForKey:@"cellSelected"] description] componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] componentsJoinedByString:@""];
    condition = [[tmp componentsSeparatedByCharactersInSet:unwantedChars] componentsJoinedByString:@""];
    
    if (![condition isEqual:@""]){
        NSString *entityID = @"http";
        NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
        fetch.entity = [NSEntityDescription entityForName:@"Entity" inManagedObjectContext:context];
        fetch.predicate = [NSPredicate predicateWithFormat:@"cellSelected CONTAINS %@", entityID];
        NSArray *array = [context executeFetchRequest:fetch error:nil];
        
        for (NSManagedObject *managedObject in array) {
            [context deleteObject:managedObject];
        }
        NSError *error = nil;
        if(![context save:&error]){
            NSLog(@"Can't save! %@ %@", error, [error localizedDescription]);
        }
    }
    
    //[newData setValue:nil forKey:@"cellSelected"];
    localStorage = [[context executeFetchRequest:fetchRequest error:nil] mutableCopy]; //check
    [newData setValue:data forKey:@"cellSelected"];
     error = nil;
    if(![context save:&error]){
        NSLog(@"Can't save! %@ %@", error, [error localizedDescription]);
    }
    localStorage = [[context executeFetchRequest:fetchRequest error:nil] mutableCopy]; //check
    [self performSegueWithIdentifier:@"showArticles" sender:sender];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getUrlsFromSite{
    webSitesURL = [NSArray arrayWithObjects:
             @"http://feeds.dzone.com/home",
             @"http://feeds.dzone.com/publications",
             @"http://feeds.dzone.com/agile",
             @"http://feeds.dzone.com/big-data",
             @"http://feeds.dzone.com/cloud",
             @"http://feeds.dzone.com/database",
             @"http://feeds.dzone.com/devops",
             @"http://feeds.dzone.com/integration",
             @"http://feeds.dzone.com/iot",
             @"http://feeds.dzone.com/java",
             @"http://feeds.dzone.com/mobile",
             @"http://feeds.dzone.com/performance",
             @"http://feeds.dzone.com/webdev",
             @"https://habrahabr.ru/rss/interesting/", nil];
    webSitesName = [NSArray arrayWithObjects:
                     @"Home",
                     @"Refcardz and Guides",
                     @"Agile Zone",
                     @"Big Data Zone",
                     @"Cloud Zone",
                     @"Database Zone",
                     @"DevOps Zone",
                     @"Integration Zone",
                     @"IoT Zone",
                     @"Java Zone",
                     @"Mobile Zone",
                     @"Performance Zone",
                     @"Web Dev Zone",
                     @"habrahabr", nil];
    cellSelected = [NSMutableArray array];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showArticles"])
    {
        ArticlesViewController *destViewController = segue.destinationViewController;
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        //NSManagedObject *object = [[self fetchedResultsController] objectAtIndexPath:indexPath];
        NSMutableArray *urlsOfSelectedFeeds = [NSMutableArray array];
        
        for (NSIndexPath *curCellPath in self.cellSelected){
            [urlsOfSelectedFeeds addObject:[webSitesURL objectAtIndex:curCellPath.row]];
        }
        
        destViewController.urlsOfArticles = urlsOfSelectedFeeds;
    }
}

#pragma mark - Source Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return webSitesURL.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *myIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:myIdentifier];
    
    NSString *curName = [webSitesName objectAtIndex:indexPath.row];
    cell.textLabel.text = [curName copy];
    if([cellSelected containsObject:indexPath]){
        NSArray *tmp = cellSelected;
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([cellSelected containsObject:indexPath]) {
        [cellSelected removeObject:indexPath];
    } else {
        [cellSelected addObject:indexPath];
    }
    [tableView reloadData];
}

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]){
        context = [delegate managedObjectContext];
    }
    return context;
}


@end
