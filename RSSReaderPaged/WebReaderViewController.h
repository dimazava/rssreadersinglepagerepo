//
//  WebReaderViewController.h
//  RSSReaderPaged
//
//  Created by Dmitriy Zawadskiy on 08.03.16.
//  Copyright © 2016 zuzex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface WebReaderViewController : UIViewController

@property (strong, nonatomic) NSString *urlOfPage;

@property (strong, nonatomic) IBOutlet UIWebView *userWebView;


@end
