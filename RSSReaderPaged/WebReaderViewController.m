//
//  WebReaderViewController.m
//  RSSReaderPaged
//
//  Created by Dmitriy Zawadskiy on 08.03.16.
//  Copyright © 2016 zuzex. All rights reserved.
//

#import "WebReaderViewController.h"

@interface WebReaderViewController ()

@end

@implementation WebReaderViewController

@synthesize userWebView;
@synthesize urlOfPage;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        NSDictionary *dataToShow = [self getJsonFromUrl];
        if (dataToShow != nil){
            NSMutableString *title = [NSMutableString stringWithFormat:@"<h1>%@</h1>", [dataToShow valueForKey:@"title"]];
            NSMutableString *content = [dataToShow valueForKey:@"content"];
            [title appendString:content];
            [userWebView loadHTMLString:title baseURL:nil];
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });
    // Do any additional setup after loading the view.
}

-(NSDictionary*)getJsonFromUrl{
    NSMutableString *url = [urlOfPage mutableCopy];
    NSString *token = @"ad1e847c8a6b8d535990dd5c1738bc9c36c47cba";
    
//    NSRange replaceRange = [url rangeOfString:@"\n"];
//    if (replaceRange.location != NSNotFound){
//        url = (NSMutableString*)[url stringByReplacingCharactersInRange:replaceRange withString:@""];
//    }
//    replaceRange = [url rangeOfString:@"\t\t"];
//    if (replaceRange.location != NSNotFound){
//        url = (NSMutableString*)[url stringByReplacingCharactersInRange:replaceRange withString:@""];
//    }
    url = (NSMutableString*)[[url componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] componentsJoinedByString:@""];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.readability.com/api/content/v1/parser?url=%@&token=%@", url, token]]];
    NSError *error = nil;
    NSHTTPURLResponse *responseCode = nil;
    NSData *oResponseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&responseCode error:&error];
    
    if ([responseCode statusCode] != 200) {
        NSLog(@"Error getting %@, HTTP status code &i", url, [responseCode statusCode]);
        return nil;
    }
    
    NSData *aData = [[[NSString alloc] initWithData:oResponseData encoding:NSUTF8StringEncoding] dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonReturn = [[NSDictionary alloc] init];
    jsonReturn = [NSJSONSerialization JSONObjectWithData:aData options:NSJSONReadingMutableContainers error:&error];
    
    return jsonReturn;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
