//
//  main.m
//  RSSReaderPaged
//
//  Created by Mac on 22.02.16.
//  Copyright © 2016 zuzex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
